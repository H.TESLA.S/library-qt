#ifndef BASECOLLECTION_H
#define BASECOLLECTION_H
#include "stdafx.h"
template<class collection_t>
class BaseCollection
{
protected:
    QString _database_file;//Database file name
    collection_t _collection;
public:
    BaseCollection(const string& file)
        :_database_file(file.c_str())
    {
        load();
    }
    virtual ~BaseCollection() noexcept
    {
        save();
    }
    virtual collection_t load()
    {
        try
        {
            this->_collection.clear();
            QFile file(this->_database_file);
            if(!file.exists())
            {
                return this->_collection;
            }
            if (!file.open(QIODevice::ReadOnly))
            {
                throw "Could not write to file: " + this->_database_file.toStdString() + "\nError: " + file.errorString().toStdString();
            }
            QDataStream in(&file);
            in.setVersion(QDataStream::Qt_5_3);
            in >> this->_collection;
            return this->_collection;
        }
        catch(exception& e)
        {
            alert(e.what());
            return this->_collection;
        }
        catch(string& e)
        {
            alert(e);
            return this->_collection;
        }
        catch(...)
        {
            return this->_collection;
        }
    }
    virtual void save()
    {
        try
        {
            QFile file(this->_database_file);
            if (!file.open(QIODevice::WriteOnly))
            {
                throw "Could not read from file: " + this->_database_file.toStdString() + "\nError: " + file.errorString().toStdString();
            }
            QDataStream out(&file);
            out.setVersion(QDataStream::Qt_5_3);
            out << this->_collection;
        }
        catch(exception& e)
        {
            alert(e.what());
        }
        catch(string& e)
        {
            alert(e);
        }
    }
    inline collection_t* collection()
    {
        return &this->_collection;
    }
};

#endif // BASECOLLECTION_H
