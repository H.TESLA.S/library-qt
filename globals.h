#ifndef GLOBALS_H
#define GLOBALS_H

#include "usercollection.h"
#include "bookcollection.h"
#include "categorycollection.h"

extern UserCollection g_user_collection;
extern BookCollection g_book_collection;
extern CategoryCollection g_category_collection;

#endif // GLOBALS_H
