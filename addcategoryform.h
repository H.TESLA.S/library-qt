#ifndef ADDCATEGORYFORM_H
#define ADDCATEGORYFORM_H

#include "basemainwindow.h"

namespace Ui
{
  class AddCategoryForm;
}

class AddCategoryForm : public BaseMainWindow
{
  Q_OBJECT

public:
  explicit AddCategoryForm(QWidget *parent = 0);
  ~AddCategoryForm();

private slots:
  void on_return_btn_clicked();

  void on_submit_btn_clicked();

private:
  Ui::AddCategoryForm *ui;
};

#endif // ADDCATEGORYFORM_H
