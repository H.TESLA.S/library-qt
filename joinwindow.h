#ifndef JOINWINDOW_H
#define JOINWINDOW_H
#include "basemainwindow.h"

namespace Ui
{
  class JoinWindow;
}

class JoinWindow : public BaseMainWindow
{
  Q_OBJECT

public:
  explicit JoinWindow(QWidget *parent = 0);
  ~JoinWindow();

private slots:
  void on_signin_btn_clicked();

  void on_return_btn_clicked();

private:
  Ui::JoinWindow *ui;
};

#endif // JOINWINDOW_H
