#include "joinwindow.h"
#include "ui_joinwindow.h"
#include "mainwindow.h"
#include "globals.h"
#include "workspacewindow.h"

JoinWindow::JoinWindow(QWidget *parent) :
  BaseMainWindow(parent),
  ui(new Ui::JoinWindow)
{
  ui->setupUi(this);
  centerize();
  this->setWindowIcon(QIcon(":/Images/Pictures/icon.ico"));
  this->setFixedSize(555,309);
  ui->warning_label->setText("");
}
void JoinWindow::on_signin_btn_clicked()
{
  if(this->ui->password_txt->text() != this->ui->password_conf_txt->text())
  {
    ui->warning_label->setText("رمز عبور با تکرار آن یکسان باید باشد!");
    return;
  }
  User user(this->ui->username_txt->text().toStdString(), this->ui->password_txt->text().toStdString());
  auto uc = (*g_user_collection.collection());
  if(uc.find(user.username().toStdString()) != uc.end())
  {
    ui->warning_label->setText("قبلا کاربری با این نام کاربری ثبت‌نام کرده است!");
    return;
  }
  g_user_collection.add(user);
  ui->warning_label->setText("کاربر جدید با موفقیت ثبت‌نام گردید!");
  User::current_user = user;
  this->close();
  WorkspaceWindow* ws= new WorkspaceWindow((MainWindow*)this->parent());
  ws->show();
}

void JoinWindow::on_return_btn_clicked()
{
  ((MainWindow*)parent())->show();
  this->close();
}

JoinWindow::~JoinWindow()
{
  delete ui;
}

