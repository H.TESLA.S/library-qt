#ifndef USERCOLLECTION_H
#define USERCOLLECTION_H
#include "stdafx.h"
#include "basecollection.h"
#include "user.h"

class UserCollection : public BaseCollection<QMap<string, User>>
{
public:
  UserCollection(const string&);
  User& find(const string&);
  bool exists(const string&);
  bool match(const User&);
  void add(const User&);
  void remove(const User&);
};

#endif // USERCOLLECTION_H
