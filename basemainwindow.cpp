#include "basemainwindow.h"
#include "ui_basemainwindow.h"

BaseMainWindow::BaseMainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::BaseMainWindow)
{
  ui->setupUi(this);
}

BaseMainWindow::~BaseMainWindow()
{
  delete ui;
}
