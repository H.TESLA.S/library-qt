#include "bookcollection.h"

BookCollection::BookCollection(const string& file)
  : BaseCollection<QMap<string, Book*>>(file)
{ }

Book* BookCollection::find(const string& hash)
{
  return *_collection.find(hash.c_str());
}

bool BookCollection::exists(const string& hash)
{
  return _collection.find(hash.c_str()) != _collection.end();
}

void BookCollection::add(Book* book)
{
  this->_collection[book->hash()] = book;
  this->save();
}

void BookCollection::remove(Book *book)
{
  this->_collection.remove(book->hash());
  this->save();
}
