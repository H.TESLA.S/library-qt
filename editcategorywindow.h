#ifndef EDITCATEGORYWINDOW_H
#define EDITCATEGORYWINDOW_H

#include "stdafx.h"
#include <QMainWindow>

namespace Ui
{
  class EditCategoryWindow;
}

class EditCategoryWindow : public QMainWindow
{
  Q_OBJECT
  string current_title;
public:
  explicit EditCategoryWindow(QWidget *parent = 0, const string& current_title = "");
  ~EditCategoryWindow();

private slots:
  void on_return_btn_clicked();

  void on_submit_btn_clicked();

  void on_remove_btn_clicked();

private:
  Ui::EditCategoryWindow *ui;
};

#endif // EDITCATEGORYWINDOW_H
