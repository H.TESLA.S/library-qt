#include "signinwindow.h"
#include "ui_signinwindow.h"
#include "mainwindow.h"
#include "stdafx.h"
#include "globals.h"
#include "workspacewindow.h"

SigninWindow::SigninWindow(QWidget *parent) :
  BaseMainWindow(parent),
  ui(new Ui::SigninWindow)
{
  ui->setupUi(this);
  centerize();
  this->setWindowIcon(QIcon(":/Images/Pictures/icon.ico"));
  this->setFixedSize(556,261);
}
void SigninWindow::on_signin_btn_clicked()
{
  User user(ui->username_txt->text().toStdString(), ui->password_txt->text().toStdString());
  if(!g_user_collection.match(user))
  {
    ui->warning_label->setText("کاربری با این مشخصات یافت نشد!");
    return;
  }
  User::current_user = g_user_collection.find(user.username().toStdString());
  ui->warning_label->setText("کاربر با موفقیت وارد سامانه شد!");
  this->close();
  WorkspaceWindow* ws= new WorkspaceWindow((MainWindow*)this->parent());
  ws->show();
}

void SigninWindow::on_return_btn_clicked()
{
  ((MainWindow*)parent())->show();
  this->close();
}
SigninWindow::~SigninWindow()
{
  delete ui;
}

