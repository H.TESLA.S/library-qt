#include "usercollection.h"

UserCollection::UserCollection(const string& file)
  :BaseCollection<QMap<string, User>>(file)
{ }

User& UserCollection::find(const string& username)
{
  return *_collection.find(username.c_str());
}

bool UserCollection::exists(const string& username)
{
  return _collection.find(username.c_str()) != _collection.end();
}

bool UserCollection::match(const User& user)
{
  return exists(user.username().toStdString()) && find(user.username().toStdString()).password() == user.password();
}

void UserCollection::add(const User& user)
{
  this->_collection[user.username().toStdString()] = user;
  this->save();
}

void UserCollection::remove(const User& user)
{
  this->_collection.remove(user.username().toStdString());
  this->save();
}
