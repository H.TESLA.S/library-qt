#include "user.h"
#include <QCryptographicHash>

User User::current_user = User();
User::User(const User& user)
  :
    _user_name(user._user_name),
    _password(user._password),
    _type(user._type)
{
}
User::User(const string & user_name, const string &password, TYPE type, bool set_raw)
  :
    _user_name(user_name.c_str()),
    _password(hash(password)),
    _type(type)
{
  if(set_raw)
    _password = QString(password.c_str());
}

QString User::hash(const string &password)
{
  return QString(QCryptographicHash::hash((password.c_str()),QCryptographicHash::Md5).toHex());
}

QDataStream& operator<<(QDataStream& out, const User& u)
{
   return out << u.username() << u.password() << u.type();
}

QDataStream& operator>> (QDataStream& in, User::TYPE& t)
{
  qint32 _t;
  in >> _t;
  t = User::TYPE(_t);
  return in;
}

QDataStream& operator>> (QDataStream& in, User& u)
{
  User::TYPE type;
  QString user_name, password;
  in >> user_name >> password >> type;
  u = User(user_name.toStdString(), password.toStdString(), type, true);
  return in;
}
