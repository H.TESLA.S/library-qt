#ifndef CATEGORIESFORM_H
#define CATEGORIESFORM_H

#include "basemainwindow.h"
#include <QStandardItemModel>
namespace Ui
{
  class CategoriesForm;
}

class CategoriesForm : public BaseMainWindow
{
  Q_OBJECT
  QStandardItemModel* model = 0;
public:
  explicit CategoriesForm(QWidget *parent = 0);
  ~CategoriesForm();
  void update_the_list_view();

private slots:
  void on_return_btn_clicked();

  void on_add_category_btn_clicked();

  void on_listView_doubleClicked(const QModelIndex &index);

private:
  Ui::CategoriesForm *ui;
};

#endif // CATEGORIESFORM_H
