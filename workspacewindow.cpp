#include "workspacewindow.h"
#include "ui_workspacewindow.h"
#include "stdafx.h"
#include "globals.h"
#include "newbookwindow.h"
#include "categoriesform.h"
#include <QRegularExpression>

WorkspaceWindow::WorkspaceWindow(QWidget *parent) :
  BaseMainWindow(parent),
  ui(new Ui::WorkspaceWindow)
{
  ui->setupUi(this);
  centerize();
  this->setWindowIcon(QIcon(":/Images/Pictures/icon.ico"));
  this->setFixedSize(1058,706);
  for (int c = 0; c < ui->tableWidget->horizontalHeader()->count(); ++c)
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(c, QHeaderView::Stretch);
  reload_table_view();
  reload_categories_combobox();
  if(User::current_user.type() != User::ADMIN)
  {
    ui->add_book_pushButton->hide();
    ui->categories_btn->hide();
    ui->borrowed_books_btn->hide();
  }
}

void WorkspaceWindow::reload_categories_combobox()
{
  QStringList qsl;

  qsl << "";

  for(auto cat : g_category_collection.categories()) qsl << cat;

  ui->categories_comboBox->clear();

  ui->categories_comboBox->addItems(qsl);
}

void WorkspaceWindow::reload_table_view()
{
  ui->tableWidget->setRowCount(0);
  auto keys = g_book_collection.collection()->keys();
  ui->tableWidget->setColumnCount(5);
  ui->tableWidget->setRowCount(keys.size());
  for(int row = 0; row < keys.size(); ++row)
  {
    add_book_to_table(row, g_book_collection.find(keys[row]));
  }
}


void WorkspaceWindow::add_book_to_table(const int& row, const Book* const book)
{
  int col = 0;
  QTableWidgetItem* item = new QTableWidgetItem(book->_title.c_str());
  item->setData(Qt::UserRole, book->hash().c_str());
  ui->tableWidget->setItem(row, col++, item);
  item = new QTableWidgetItem(book->_publisher.c_str());
  item->setData(Qt::UserRole, book->hash().c_str());
  ui->tableWidget->setItem(row, col++, item);
  item = new QTableWidgetItem(book->_authors.c_str());
  item->setData(Qt::UserRole, book->hash().c_str());
  ui->tableWidget->setItem(row, col++, item);
  item = new QTableWidgetItem(book->_translators.c_str());
  item->setData(Qt::UserRole, book->hash().c_str());
  ui->tableWidget->setItem(row, col++, item);
  item = new QTableWidgetItem(book->_published_date.c_str());
  item->setData(Qt::UserRole, book->hash().c_str());
  ui->tableWidget->setItem(row, col++, item);
}


WorkspaceWindow::~WorkspaceWindow()
{
  delete ui;
}

void WorkspaceWindow::on_add_book_pushButton_clicked()
{
  auto nbw = new NewBookWindow(this);
  nbw->show();
}

void WorkspaceWindow::on_categories_btn_clicked()
{
  auto cf = new CategoriesForm(this);
  cf->show();
}

void WorkspaceWindow::on_tableWidget_doubleClicked(const QModelIndex &index)
{
  auto nbw = new NewBookWindow(this, g_book_collection.find(ui->tableWidget->model()->itemData(index)[Qt::UserRole].toString().toStdString()));
  nbw->show();
}

void WorkspaceWindow::on_logout_btn_clicked()
{
  ((QMainWindow*)parent())->show();
  close();
}

void WorkspaceWindow::on_categories_comboBox_currentIndexChanged(const QString &cat)
{
  ui->tableWidget->setRowCount(0);
  if(cat.length() == 0)
  {
    reload_table_view();
    return;
  }
  auto books = g_category_collection.list_category(cat.toStdString());
  ui->tableWidget->setColumnCount(5);
  ui->tableWidget->setRowCount(books.size());
  QSet<QString> hashes;
  for(int row = 0; row < books.size(); ++row)
  {
    if(hashes.find(books[row]->hash().c_str()) != hashes.end()) continue;
    hashes.insert(books[row]->hash().c_str());
    add_book_to_table(row, books[row]);
  }
}

void WorkspaceWindow::on_search_pushButton_clicked()
{
  ui->tableWidget->setRowCount(0);
  auto search_term = ui->search_lineEdit->text();
  auto books = g_book_collection.collection()->values();
  if(search_term.length() == 0)
  {
    reload_table_view();
    return;
  }
  QRegularExpression re(search_term, QRegularExpression::CaseInsensitiveOption);
  ui->tableWidget->setColumnCount(5);
  QSet<QString> hashes;
  for(int row = 0, count = 1; row < books.size(); ++row)
  {
    if(hashes.find(books[row]->hash().c_str()) != hashes.end()) continue;
    hashes.insert(books[row]->hash().c_str());
    bool should_add =
        re.match(books[row]->_title.c_str()).hasMatch() ||
        re.match(books[row]->_desc.c_str()).hasMatch() ||
        re.match(books[row]->_authors.c_str()).hasMatch() ||
        re.match(books[row]->_translators.c_str()).hasMatch() ||
        re.match(books[row]->_published_date.c_str()).hasMatch() ||
        re.match(books[row]->_published_no.c_str()).hasMatch() ||
        re.match(books[row]->_publisher.c_str()).hasMatch() ||
        re.match(books[row]->_authors.c_str()).hasMatch();
    for(auto b : books[row]->_borrowed_to)
      should_add = should_add || re.match(b).hasMatch();
    if(should_add)
    {
      ui->tableWidget->setRowCount(count + 1);
      add_book_to_table(count++, books[row]);
    }
  }
  ui->tableWidget->removeRow(0);
}

void WorkspaceWindow::on_my_borrowed_book_pushButton_clicked()
{
  ui->search_lineEdit->setText(User::current_user.username());
  on_search_pushButton_clicked();
}

void WorkspaceWindow::on_borrowed_books_btn_clicked()
{
  ui->tableWidget->setRowCount(0);
  auto books = g_book_collection.collection()->values();
  ui->tableWidget->setColumnCount(5);
  QSet<QString> hashes;
  for(int row = 0, count = 1; row < books.size(); ++row)
  {
    if(hashes.find(books[row]->hash().c_str()) != hashes.end()) continue;
    hashes.insert(books[row]->hash().c_str());
    if(books[row]->_borrowed_to.size())
    {
      ui->tableWidget->setRowCount(count + 1);
      add_book_to_table(count++, books[row]);
    }
  }
  ui->search_lineEdit->setText("borrowed");
  ui->tableWidget->removeRow(0);
}
