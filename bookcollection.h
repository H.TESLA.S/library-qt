#ifndef BOOKCOLLECTION_H
#define BOOKCOLLECTION_H
#include "stdafx.h"
#include "basecollection.h"
#include "book.h"

class BookCollection : public BaseCollection<QMap<string, Book*>>
{
public:
  BookCollection(const string&);
  Book * find(const string&);
  bool exists(const string&);
  void add(Book*);
  void remove(Book*);
};

#endif // BOOKCOLLECTION_H
