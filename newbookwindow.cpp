#include "newbookwindow.h"
#include "ui_newbookwindow.h"
#include "stdafx.h"
#include "globals.h"
#include "workspacewindow.h"

NewBookWindow::NewBookWindow(QWidget *parent, Book* book) :
  BaseMainWindow(parent),
  _book(book),
  ui(new Ui::NewBookWindow)
{
  ui->setupUi(this);
  centerize();
  this->setWindowIcon(QIcon(":/Images/Pictures/icon.ico"));
  this->setFixedSize(800,996);
  update_the_list_view();
  if(book)
  {
    ui->title_txt->setText(book->_title.c_str());
    ui->desc_txt->setPlainText(book->_desc.c_str());
    ui->publisher_txt->setText(book->_publisher.c_str());
    ui->authors_txt->setText(book->_authors.c_str());
    ui->translators_txt->setText(book->_translators.c_str());
    ui->published_date_txt->setDate(QDate::fromString(book->_published_date.c_str(), "yyyy"));
    ui->published_no->setValue(atoll(book->_published_no.c_str()));
    ui->available_count_txt->setValue(book->_avail_count);
    // select categories of the book
    for(auto c : book->_categories)
    {
      QModelIndexList indices = model->match(
        model->index(0,0),  // first row, first column
        Qt::DisplayRole,  // search the text as displayed
        c,  // there is a QVariant(QString) conversion constructor
        -1, // all matched data
        Qt::MatchExactly // or Qt::MatchFixedString
      );
      for(auto index : indices)
      {
        ui->listView->selectionModel()->select(index, QItemSelectionModel::Select);
      }
    }
    disable_all(true);
    ui->submit_pushButton->hide();
    if(this->_book->has_borrowed(User::current_user.username().toStdString()))
    {
      ui->takingit_pushButton->hide();
      ui->returningit_pushButton->show();
    }
    else
    {
      ui->takingit_pushButton->show();
      ui->returningit_pushButton->hide();
    }
    if(User::current_user.type() == User::ADMIN)
    {
      auto _model = new QStandardItemModel(book->_borrowed_to.size(), 1);
      _model->clear();
      for(auto u : book->_borrowed_to)
      {
        _model->appendRow(new QStandardItem(u));
      }
      ui->borrow_list->setModel(_model);
    }
    else
    {
      ui->borrow_list->hide();
      ui->borrow_list_label->hide();
      ui->delete_pushButton->hide();
      ui->edit_pushButton->hide();
    }
  }
  else
  {
    ui->edit_pushButton->hide();
    ui->takingit_pushButton->hide();
    ui->returningit_pushButton->hide();
    ui->delete_pushButton->hide();
  }
}

void NewBookWindow::disable_all(bool disable)
{
  ui->title_txt->setEnabled(!disable);
  ui->desc_txt->setEnabled(!disable);
  ui->publisher_txt->setEnabled(!disable);
  ui->authors_txt->setEnabled(!disable);
  ui->translators_txt->setEnabled(!disable);
  ui->published_date_txt->setEnabled(!disable);
  ui->published_no->setEnabled(!disable);
  ui->available_count_txt->setEnabled(!disable);
  ui->listView->setEnabled(!disable);
}

void NewBookWindow::update_the_list_view()
{
  auto cats = g_category_collection.categories();
  if(!model)
    model = new QStandardItemModel(cats.size(), 1);
  model->clear();
  for(auto cat : cats)
  {
    model->appendRow(new QStandardItem(cat));
  }
  ui->listView->setModel(model);
}


NewBookWindow::~NewBookWindow()
{
  delete ui;
}

void NewBookWindow::on_return_pushButton_clicked()
{
  ((QMainWindow*)parent())->show();
  this->close();
}

void NewBookWindow::on_submit_pushButton_clicked()
{
  if(_book)
  {
    for(auto cat : _book->_categories)
      g_category_collection.unbind_from_category(cat.toStdString(), { _book }, true);
    g_book_collection.remove(_book);
  }
  auto b = new Book(
        ui->title_txt->text().toStdString(),
        ui->desc_txt->toPlainText().toStdString(),
        ui->publisher_txt->text().toStdString(),
        ui->authors_txt->text().toStdString(),
        ui->translators_txt->text().toStdString(),
        ui->published_date_txt->text().toStdString(),
        ui->published_no->text().toStdString(),
        (quint64)atol(ui->available_count_txt->text().toStdString().c_str())
      );
  for(auto& index : ui->listView->selectionModel()->selectedIndexes())
  {
      b->bind_to_category(model->itemFromIndex(index)->text().toStdString());
  }
  g_book_collection.add(b);
  ((WorkspaceWindow*)parent())->reload_table_view();
  alert("The book has been saved successfully!");
  this->close();
}

void NewBookWindow::on_edit_pushButton_clicked()
{
    disable_all(false);
    ui->edit_pushButton->hide();
    ui->takingit_pushButton->hide();
    ui->returningit_pushButton->hide();
    ui->delete_pushButton->hide();
    ui->submit_pushButton->show();
}

void NewBookWindow::on_takingit_pushButton_clicked()
{
  _book->borrow_to(User::current_user.username().toStdString());
  ui->takingit_pushButton->hide();
  ui->returningit_pushButton->show();
}

void NewBookWindow::on_returningit_pushButton_clicked()
{
  _book->due_return(User::current_user.username().toStdString());
  ui->takingit_pushButton->show();
  ui->returningit_pushButton->hide();
}

void NewBookWindow::on_delete_pushButton_clicked()
{
  g_book_collection.remove(_book);
  alert("The book has been deleted successfully!");
  ((WorkspaceWindow*)parent())->reload_table_view();
  close();
}
