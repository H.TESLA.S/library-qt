#ifndef SIGNINWINDOW_H
#define SIGNINWINDOW_H

#include "basemainwindow.h"

namespace Ui {
  class SigninWindow;
}

class SigninWindow : public BaseMainWindow
{
  Q_OBJECT

public:
  explicit SigninWindow(QWidget *parent = 0);
  ~SigninWindow();

private slots:
  void on_signin_btn_clicked();

  void on_return_btn_clicked();

private:
  Ui::SigninWindow *ui;
};

#endif // SIGNINWINDOW_H
