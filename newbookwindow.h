#ifndef NEWBOOKWINDOW_H
#define NEWBOOKWINDOW_H

#include <QStandardItemModel>
#include "book.h"
#include "basemainwindow.h"

namespace Ui
{
  class NewBookWindow;
}

class NewBookWindow : public BaseMainWindow
{
  Q_OBJECT
  Book* _book = 0;
  QStandardItemModel* model = 0;
public:
  explicit NewBookWindow(QWidget *parent = 0, Book* = 0);
  ~NewBookWindow();
  void update_the_list_view();

private slots:

  void on_return_pushButton_clicked();

  void on_submit_pushButton_clicked();

  void on_edit_pushButton_clicked();

  void on_takingit_pushButton_clicked();

  void on_returningit_pushButton_clicked();

  void on_delete_pushButton_clicked();

private:
  Ui::NewBookWindow *ui;
  void disable_all(bool);
};

#endif
