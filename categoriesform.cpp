#include "categoriesform.h"
#include "ui_categoriesform.h"
#include "addcategoryform.h"
#include "editcategorywindow.h"
#include "globals.h"

CategoriesForm::CategoriesForm(QWidget *parent) :
  BaseMainWindow(parent),
  ui(new Ui::CategoriesForm)
{
  ui->setupUi(this);
  centerize();
  this->setWindowIcon(QIcon(":/Images/Pictures/icon.ico"));
  this->setFixedSize(590,377);
  update_the_list_view();
}

void CategoriesForm::update_the_list_view()
{
  auto cats = g_category_collection.categories();
  if(!model)
    model = new QStandardItemModel(cats.size(), 1);
  model->clear();
  for(auto cat : cats)
  {
    model->appendRow(new QStandardItem(cat));
  }
  ui->listView->setModel(model);
}

CategoriesForm::~CategoriesForm()
{
  delete ui;
}
void CategoriesForm::on_return_btn_clicked()
{
  this->close();
}

void CategoriesForm::on_add_category_btn_clicked()
{
  auto acf = new AddCategoryForm(this);
  acf->show();
}

void CategoriesForm::on_listView_doubleClicked(const QModelIndex &index)
{
  auto item = model->index(index.row(), index.column()).data().toString().toStdString();
  auto ecw = new EditCategoryWindow(this, item);
  ecw->show();
}
