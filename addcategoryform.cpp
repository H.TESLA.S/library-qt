#include "addcategoryform.h"
#include "ui_addcategoryform.h"
#include "globals.h"
#include "categoriesform.h"
#include "workspacewindow.h"

AddCategoryForm::AddCategoryForm(QWidget *parent) :
  BaseMainWindow(parent),
  ui(new Ui::AddCategoryForm)
{
  ui->setupUi(this);
  centerize();
  this->setWindowIcon(QIcon(":/Images/Pictures/icon.ico"));
  this->setFixedSize(609,198);
}

AddCategoryForm::~AddCategoryForm()
{
  delete ui;
}

void AddCategoryForm::on_return_btn_clicked()
{
  close();
}

void AddCategoryForm::on_submit_btn_clicked()
{
  g_category_collection.add_category(ui->category_txt->text().toStdString());
  ((CategoriesForm*)this->parent())->update_the_list_view();
  alert("category saved successfully!");
  close();
}
