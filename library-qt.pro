QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = library-qt
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += \
        main.cpp \
        mainwindow.cpp \
    joinwindow.cpp \
    signinwindow.cpp \
    user.cpp \
    usercollection.cpp \
    stdafx.cpp \
    globals.cpp \
    workspacewindow.cpp \
    newbookwindow.cpp \
    book.cpp \
    bookcollection.cpp \
    categorycollection.cpp \
    addcategoryform.cpp \
    categoriesform.cpp \
    basemainwindow.cpp \
    editcategorywindow.cpp
HEADERS += \
        mainwindow.h \
    joinwindow.h \
    signinwindow.h \
    user.h \
    stdafx.h \
    usercollection.h \
    globals.h \
    workspacewindow.h \
    newbookwindow.h \
    book.h \
    bookcollection.h \
    categorycollection.h \
    basecollection.h \
    addcategoryform.h \
    categoriesform.h \
    basemainwindow.h \
    editcategorywindow.h
FORMS += \
        mainwindow.ui \
    joinwindow.ui \
    signinwindow.ui \
    workspacewindow.ui \
    newbookwindow.ui \
    addcategoryform.ui \
    categoriesform.ui \
    basemainwindow.ui \
    editcategorywindow.ui

RESOURCES += \
    resource.qrc
