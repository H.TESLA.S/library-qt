#include "stdafx.h"
#include <QApplication>

void alert(const string& message)
{
  QMessageBox::information(QApplication::focusWidget(), "!توجه", message.c_str());
}

QDataStream& operator<< (QDataStream& out, const string& s)
{
   return out << QString(s.c_str());
}

QDataStream& operator>> (QDataStream& in, string& s)
{
  QString str;
  in >> str;
  s = str.toStdString();
  return in;
}
