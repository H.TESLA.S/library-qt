#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "basemainwindow.h"
namespace Ui
{
  class MainWindow;
}

class MainWindow : public BaseMainWindow
{
  Q_OBJECT
public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:

  void on_join_btn_2_clicked();

  void on_signin_btn_clicked();

private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
