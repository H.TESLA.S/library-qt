#ifndef USER_H
#define USER_H
#include "stdafx.h"

class User
{
public:
  enum TYPE { ADMIN, NORMAL };
  static User current_user;
public:
  User() { }//defult constructor
  User(const User&);//Copy constructor
  User(const string&, const string&, TYPE = NORMAL, bool = false);//parametric constructor
  inline QString password() const { return this->_password; }
  inline QString username() const { return this->_user_name; }
  inline TYPE type() const { return this->_type; }
protected:
  QString hash(const string&);
private:
  QString
    _user_name,
    _password;
  TYPE _type;
};
QDataStream& operator<<(QDataStream&, const User&);
QDataStream& operator>>(QDataStream&, User&);
QDataStream& operator>>(QDataStream&, User::TYPE&);

#endif // USER_H
