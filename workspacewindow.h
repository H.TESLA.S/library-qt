#ifndef WORKSPACEWINDOW_H
#define WORKSPACEWINDOW_H

#include <QStandardItemModel>

#include "basemainwindow.h"
#include "book.h"

namespace Ui {
  class WorkspaceWindow;
}

class WorkspaceWindow : public BaseMainWindow
{
  Q_OBJECT

  QStandardItemModel* model;

public:
  explicit WorkspaceWindow(QWidget *parent = 0);
  ~WorkspaceWindow();
  void reload_table_view();
  void reload_categories_combobox();
  void add_book_to_table(const int&, const Book* const);

private slots:
  void on_add_book_pushButton_clicked();

  void on_categories_btn_clicked();

  void on_tableWidget_doubleClicked(const QModelIndex &index);

  void on_logout_btn_clicked();

  void on_categories_comboBox_currentIndexChanged(const QString &arg1);

  void on_search_pushButton_clicked();

  void on_my_borrowed_book_pushButton_clicked();

  void on_borrowed_books_btn_clicked();

private:
  Ui::WorkspaceWindow *ui;
};

#endif // WORKSPACEWINDOW_H
