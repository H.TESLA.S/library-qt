 #ifndef STDAFX_H
#define STDAFX_H

#include <time.h>
#include <string>
#include <random>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <assert.h>
#include <iostream>
#include <exception>
#include <algorithm>
#include <functional>
#include <type_traits>
using namespace std;
#include <QMap>
#include <QFile>
#include <QList>
#include <QString>
#include <QDataStream>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QStandardItemModel>
#include <QPixmap>
void alert(const string&);
QDataStream& operator<<(QDataStream&, const string&);
QDataStream& operator>>(QDataStream&, string&);

#endif // STDAFX_H
