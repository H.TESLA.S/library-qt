#ifndef BOOK_H
#define BOOK_H
#include "stdafx.h"
#include <QSet>
class Book
{
public:
  string
    _title,
    _desc,
    _publisher,
    _authors,
    _translators,
    _published_date,
    _published_no;
  quint64
    _avail_count;
  QSet<QString>
    _categories;
  QSet<QString>
    _borrowed_to;
public:
  Book(){ }
  Book(
      const string &title,
      const string &desc,
      const string &publisher,
      const string &authors,
      const string &tranlators,
      const string &published_date,
      const string &published_no,
      const size_t &avail_count);
  void borrow_to(const string&);
  void due_return(const string&);
  bool has_borrowed(const string&) const;
  void bind_to_category(const string&, bool = false);
  void unbind_from_category(const string&, bool = false);
  string hash() const;
};
QDataStream& operator<<(QDataStream&, const Book* const);
QDataStream& operator>>(QDataStream&, Book*&);
#endif // BOOK_H
