#ifndef CATEGORYCOLLECTION_H
#define CATEGORYCOLLECTION_H
#include "stdafx.h"
#include "book.h"
#include "basecollection.h"

class CategoryCollection : public BaseCollection<QMap<string, QList<Book*>>>
{
public:
  CategoryCollection(const string&);
  void save() { }
  QMap<string, QList<Book*>> load() { return _collection; }
  bool exists(const string&) const;
  void add_category(const string&);
  void bind_to_category(const string&, const QList<Book*>&, bool = false);
  void unbind_from_category(const string&, const QList<Book*>&, bool = false);
  void rename(const string&, const string&);
  void remove(const string&);
  inline QList<Book*> list_category(const string& cat)
  {
      if(!exists(cat))
      {
          add_category(cat);
      }
    return _collection.value(cat);
  }
  inline QSet<QString> categories() const
  {
    auto out = QSet<QString>();
    for(auto k : _collection.keys())
    {
        out << QString(k.c_str());
    }
    return out;
  }
};

#endif // CATEGORYCOLLECTION_H
