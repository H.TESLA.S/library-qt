#ifndef BASEMAINWINDOW_H
#define BASEMAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QDesktopWidget>
namespace Ui
{
  class BaseMainWindow;
}

class BaseMainWindow : public QMainWindow
{
  Q_OBJECT
public:
  explicit BaseMainWindow(QWidget *parent = 0);
  ~BaseMainWindow();

private:
  Ui::BaseMainWindow *ui;

protected:
  inline void centerize()
  {
    move(QApplication::desktop()->screen()->rect().center() - this->rect().center());
  }


};
#endif // BASEMAINWINDOW_H
