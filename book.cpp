#include "book.h"
#include "globals.h"

Book::Book(
  const string &title,
  const string &desc,
  const string &publisher,
  const string &authors,
  const string &tranlators,
  const string &published_date,
  const string &published_no,
  const size_t &avail_count)
  :
    _title(title),
    _desc(desc),
    _publisher(publisher),
    _authors(authors),
    _translators(tranlators),
    _published_date(published_date),
    _published_no(published_no),
    _avail_count(avail_count)
{
}

void Book::borrow_to(const string& username)
{
  this->_borrowed_to << username.c_str();
}

void Book::due_return(const string& username)
{
  this->_borrowed_to.remove(username.c_str());
}

bool Book::has_borrowed(const string& username) const
{
  for(auto u : this->_borrowed_to)
    if(u.toStdString() == username)
      return true;
  return false;
}

void Book::bind_to_category(const string& cat, bool skip_category_collection)
{
  _categories << QString(cat.c_str());
  if(!skip_category_collection)
    g_category_collection.bind_to_category(cat, { this }, true);
}

void Book::unbind_from_category(const string& cat, bool skip_category_collection)
{
  _categories.remove(QString(cat.c_str()));
  if(!skip_category_collection)
    g_category_collection.unbind_from_category(cat, { this }, true);
}

string Book::hash() const
{
  return _title + "-" + _publisher + "-" + _authors;
}

QDataStream& operator<<(QDataStream& out, const Book* const b)
{
  return out
    << b->_title
    << b->_desc
    << b->_publisher
    << b->_authors
    << b->_translators
    << b->_published_date
    << b->_published_no
    << b->_avail_count
    << b->_categories
    << b->_borrowed_to;
}

QDataStream& operator>>(QDataStream& in, Book*& b)
{
  QString
    _title,
    _desc,
    _publisher,
    _authors,
    _translators,
    _published_date,
    _published_no;
  quint64
    _avail_count;
  QSet<QString>
    _categories;
  QSet<QString>
    _borrowed_to;
  in
    >> _title
    >> _desc
    >> _publisher
    >> _authors
    >> _translators
    >> _published_date
    >> _published_no
    >> _avail_count
    >> _categories
    >> _borrowed_to;
  b = new Book(
        _title.toStdString(),
        _desc.toStdString(),
        _publisher.toStdString(),
        _authors.toStdString(),
        _translators.toStdString(),
        _published_date.toStdString(),
        _published_no.toStdString(),
        _avail_count);
  b->_categories = _categories;
  b->_borrowed_to = _borrowed_to;
  for(auto c : b->_categories)
  {
      g_category_collection.bind_to_category(c.toStdString(), { b }, true);
  }
  return in;
}
