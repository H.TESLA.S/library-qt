#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "stdafx.h"
#include "joinwindow.h"
#include "signinwindow.h"
#include "globals.h"
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
  BaseMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  centerize();
  this->setWindowIcon(QIcon(":/Images/Pictures/icon.ico"));
  this->setFixedSize(448,207);
  if(!g_user_collection.exists("admin"))
    g_user_collection.add(User("admin", "admin", User::ADMIN));
}
void MainWindow::on_join_btn_2_clicked()
{
  auto join = new JoinWindow(this);
  join->show();
  this->hide();
}

void MainWindow::on_signin_btn_clicked()
{
  auto signin = new SigninWindow(this);
  signin->show();
  this->hide();
}
MainWindow::~MainWindow()
{
  delete ui;
}

