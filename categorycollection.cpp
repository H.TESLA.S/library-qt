#include "categorycollection.h"
CategoryCollection::CategoryCollection(const string& file)
  :BaseCollection<QMap<string, QList<Book*>>>(file)
{ }
void CategoryCollection::add_category(const string& cat)
{
  if(exists(cat))
    return;
  _collection[cat] = QList<Book*>();
  save();
}
void CategoryCollection::rename(const string& _old, const string& _new)
{
  bind_to_category(_new, _collection.value(_old));
  remove(_old);
}

void CategoryCollection::remove(const string& _old)
{
  unbind_from_category(_old, _collection.value(_old));
  _collection.remove(_old);
}
void CategoryCollection::bind_to_category(const string& cat, const QList<Book*>& books, bool skip_book_category_set)
{
  if(!exists(cat))
    add_category(cat);
  for(auto b : books)
  {
    _collection[cat] << b;
    if(!skip_book_category_set)
      b->bind_to_category(cat, true);
  }
  save();
}
void CategoryCollection::unbind_from_category(const string& cat, const QList<Book*>& books, bool skip_book_category_set)
{
  for(auto b : books)
  {
    auto _vals = _collection[cat];
    auto it = _vals.begin();
    while(it != _vals.end())
    {
      if((*it)->hash() == b->hash())
      {
        _collection[cat].erase(it);
        if(!skip_book_category_set)
          b->unbind_from_category(cat, true);
        break;
      }
      it++;
    }
  }
}
bool CategoryCollection::exists(const string& cat) const
{
  return _collection.find(cat.c_str()) != _collection.end();
}
