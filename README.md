# Library-management

Library-management is a C++ Application for managing a Library Which is written with the help of Qt framework

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)