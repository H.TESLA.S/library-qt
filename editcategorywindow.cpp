#include "editcategorywindow.h"
#include "ui_editcategorywindow.h"
#include "globals.h"
#include "categoriesform.h"

EditCategoryWindow::EditCategoryWindow(QWidget *parent, const string& current_title) :
  QMainWindow(parent),
  current_title(current_title),
  ui(new Ui::EditCategoryWindow)
{
  ui->setupUi(this);
  this->setFixedSize(597,209);
  this->setWindowIcon(QIcon(":/Images/Pictures/icon.ico"));
  ui->current_category_txt->setText(QString(current_title.c_str()));
}

EditCategoryWindow::~EditCategoryWindow()
{
  delete ui;
}

void EditCategoryWindow::on_return_btn_clicked()
{
  close();
}

void EditCategoryWindow::on_submit_btn_clicked()
{
  g_category_collection.rename(ui->current_category_txt->text().toStdString(), ui->new_category_txt->text().toStdString());
  ((CategoriesForm*)this->parent())->update_the_list_view();
  alert("The category successfully renamed!");
  close();
}

void EditCategoryWindow::on_remove_btn_clicked()
{
  g_category_collection.remove(ui->current_category_txt->text().toStdString());
  ((CategoriesForm*)this->parent())->update_the_list_view();
  alert("The category successfully removed!");
  close();
}
